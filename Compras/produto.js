const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Cliente = sequelize.define('Produto', {

    descricao: {
        type: DataTypes.STRING,
        allowNull: false
    },

    quantidade: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    
    preco: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },

    categoria: {
        type: DataTypes.STRING,
        allowNull: false
    },

});

Cliente.associate = function(models) {
    Cliente.hasMany(models.Produto)
};

module.exports = Loja;