const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Cliente = sequelize.define('Cliente', {

    nome: {
        type: DataTypes.STRING,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
    telefone: {
        type: DataTypes.STRING,
        allowNull: false
    },

    rg: {
        type: DataTypes.STRING,
        allowNull: false
    },

    endereco: {
        type: DataTypes.STRING,
        allowNull: false
    },
});

Cliente.associate = function(models) {
    Cliente.hasMany(models.Produto)
};

module.exports = Cliente;